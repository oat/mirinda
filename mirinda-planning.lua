  -- beatobj

c2l(0, 20):wiggle(50, 'tipsy'):onoff('invert')

-- ways to get a beatobj


-- c2l

c2l(start, ending, trunc) -- makes a chart2lua of a segment and uses that
-- start: start beat
-- ending: ending beat
-- trunc: whether to include the notes at beat (ending)

c2l(notedata) -- uses an existing chart2lua
-- notedata: notitg format notedata, like the automatically generated notedata from 'save area to lua'


-- beat

beat(table) -- raw beat input
-- table: beat table of every beat to fire on


-- forl

forl(start, ending, add) -- acts as a for loop
-- start, ending and add act like arguments to a for loop


-- example

-- standard mirin, no for loops
ease {0, 2, outCirc, 100, 'invert'}
ease {2, 2, outCirc, 0, 'invert'}
ease {4, 2, outCirc, 100, 'invert'}
ease {6, 2, outCirc, 0, 'invert'}

-- for loop
for b = 0, 6, 2 do
  local i = b/2+1
  ease {b, 2, outCirc, i%2*100, 'invert'}
end

-- mirinda
forl(0, 6, 2):sharponoff('invert')
-- or
forl(0, 6, 2):ease(outCirc):align(0):onoff('invert')


-- standard mirin, no for loops
ease {-1, 2, inOutCirc, 100, 'tipsy'}
set {0, 1, 'glitch2'}
ease {0, 1, outCirc, 0, 'glitch2'}
ease {1, 2, inOutCirc, -100, 'tipsy'}
set {2, 1, 'glitch2'}
ease {2, 1, outCirc, 0, 'glitch2'}
ease {3, 2, inOutCirc, 100, 'tipsy'}
set {4, 1, 'glitch2'}
ease {4, 1, outCirc, 0, 'glitch2'}
ease {5, 2, inOutCirc, 100, 'tipsy'}
set {6, 1, 'glitch2'}
ease {6, 1, outCirc, 0, 'glitch2'}

-- for loop
for b = 0, 6, 2 do
  local i = b/2
  local off = i%2*2-1
  ease {b - 1, 2, inOutCirc, 100 * off, 'tipsy'}
  set {b, 1, 'glitch2'}
  ease {b, 1, outCirc, 0, 'glitch2'}
end

-- mirinda
forl(0, 6, 2):kick(1, 'glitch2'):wiggle('tipsy')








-- modules time

-- example module:

plugin {
  name = 'glitchmodule',
  plr = {}
}

definemod {'glitch2', function(a)
  glitch2sprite:GetShader():uniform1f('amount', a/100)
  if a == 0 then glitch2aft:hidden(1); glitch2sprite:hidden(1) else glitch2aft:hidden(0); glitch2sprite:hidden(0) end
end}

-- file usage:

import 'glitchmodule.glitch2'
